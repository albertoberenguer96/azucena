<?php
require("pdf/fpdf.php");
require("models/pedidos_model.php");

class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
	// Logo
	//$this->Image('logoIES.jpg',10,8,33);
	// Arial bold 15
	$this->SetFont('Arial','B',15);
	
	$this->Cell(1);// Movernos a la derecha
	// T�tulo
	$this->setFillColor(128 ,128, 114);//color rgba
	//$this->Cell(100,8,'Encuesta. Resultados de la votacion',1,0,'L',true); //tama�oCelda, Vertical, Variable o texto, Borde, salto de linea, text-align
	
	// Salto de l�nea
	$this->Ln(20);
}

// Pie de p�gina
function Footer()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// N�mero de p�gina
	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creaci�n del objeto de la clase heredada
ob_start();

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',15);//Tipo letra, Decoracion(Cursiva,Negrita), Tama�o

$pedido= new Pedido();
$pedido->get();
$pedidos = $pedido->get_rows();

$pdf->Image('img/logo.png',150,10,50);
foreach ($pedidos as $indice => $fila) {

$pdf->setFillColor(251, 214, 255);
$pdf->Cell(75);
$pdf->Cell(40,10,'Pedido '.($indice+1),0,1,'C',true);
$pdf->SetFont('Arial','B',15);
$pdf->ln(10);
$pdf->Cell(94,10,'Nombre y apellidos: '.$fila['nombre']." ".$fila['apellidos'],0,1,'C');
$pdf->ln(5);
$pdf->Cell(40,10,"Tratamiento: ".$fila['descripcion'],0,1);
$pdf->Cell(40,10,"Fecha: ".$fila['fecha_ped'],0,1);
$pdf->ln(10);


 }
 	//$pdf->Image('img/apartamento9.jpg',42,100,120);//horizontal,vertical,tama�o,alto,formato,link

 $pdf->Output();
?>
