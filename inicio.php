<main>
  <a class="ir-arriba"  javascript:void(0) title="Volver arriba"><!-- Aquí irá un boton para ascender al inicio de la página -->
    <span class="fa-stack">
      <i class="fa fa-circle fa-stack-2x"></i>
      <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
    </span>
  </a>
  <!-- Aquí irá un boton para cambiar a modo oscuro o claro javascript -->
  <div id="sliderContent">
    <div class="slider"><!-- Aquí está el BxSlider de javascript -->
     <div><img src="img/mackupmod.jpg"></div>
     <div><img src="img/beauty3.jpg"></div>
     <div><img src="img/maquillate2.jpg"></div>
    </div>
    </div>

  <div id="filosofia">
    <h1>NUESTRA FILOSOFIA <i class="fas fa-brain"></i></h1>  
    <h2>La <b>BELLEZA</b> comienza con la decisión de ser uno mismo</h2>
    <p>¿Por qué Azucena?</p>
    <p>Azucena representa la pureza. En nuestras instalaciones queremos que te sientas en un viaje Astral y te encuentres con tus cinco sentidos</p>
    <p>Porque la perfección para nosotros no es una opción</p>
  </div>

  <h1 style="text-align: center;" id="services">NUESTROS SERVICIOS <i class="fab fa-leanpub"></i></h1>
  <div class="garantias">
    <h1>Garantia <i class="fas fa-award"></i></h1>
     <p>Garantia en todos nuestros tratamientos</p>
  </div>
    <div id="servicios">
      <div class="servicio">
        <h2>FACIAL</h2>
        <img src="img/facial.jpg">
        <div class="tratamiento">
          <p><i class="fas fa-check-circle"></i> Limpieza Básica</p>
          <p><i class="fas fa-check-circle"></i> Tratamiento Acné</p>
          <p><i class="fas fa-check-circle"></i> Ácido</p>
          <p><i class="fas fa-check-circle"></i> Tratamiento Lifting</p>
          <p><i class="fas fa-check-circle"></i> Tinte de Pestañas</p>
        </div>
      </div>
      <div class="servicio">
        <h2>CORPORAL</h2>
        <img src="img/corporal.jpg">
        <div class="tratamiento">
        <p><i class="fas fa-check-circle"></i> Tratamiento Corporal</p>
        <p><i class="fas fa-check-circle"></i> Peeling con Hidratación</p>
        <p><i class="fas fa-check-circle"></i> Peeling con Chocoterapia</p>
        <p><i class="fas fa-check-circle"></i> Peeling con Vinoterapia</p>
        </div> 
      </div>
      <div class="servicio">
        <h2>DEPILACION</h2>
        <img src="img/depilacion.jpg">
        <p><i class="fas fa-check-circle"></i> Piernas Enteras o Medias</p>
        <p><i class="fas fa-check-circle"></i> Cejas y Brazos</p>
        <p><i class="fas fa-check-circle"></i> Pecho o Espalda</p>
        <p><i class="fas fa-check-circle"></i> Gluteos</p>
        <p><i class="fas fa-check-circle"></i> Ingles y Axilas</p>
        <p><i class="fas fa-check-circle"></i> Labio</p> 
      </div>
      <div class="servicio">
        <h2>MANOS</h2>
        <img src="img/manos.jpg">
        <p><i class="fas fa-check-circle"></i> Manicura</p>
        <p><i class="fas fa-check-circle"></i> Francesa</p>
        <p><i class="fas fa-check-circle"></i> Uñas Gel</p>
        <p><i class="fas fa-check-circle"></i> Francesa y Decoración</p>
        <p><i class="fas fa-check-circle"></i> Semipermanente</p>
        <p><i class="fas fa-check-circle"></i> Permanente</p>
      </div>
      <div class="servicio">
        <h2>PIES</h2>
        <img src="img/pies.jpg">
        <p><i class="fas fa-check-circle"></i> Pedicura</p>
        <p><i class="fas fa-check-circle"></i> Pedicura Spa</p>
        <p><i class="fas fa-check-circle"></i> Pedicura con Esmaltado Semipermanente</p>
        <p><i class="fas fa-check-circle"></i> Pedicura con Gel</p> 
      </div>
      <div class="servicio">
        <h2>MICROPIGMENTACIÓN</h2>
        <img src="img/micropigmentacion.jpg">
        <p><i class="fas fa-check-circle"></i> Cejas</p>
        <p><i class="fas fa-check-circle"></i> Labios</p>
        <p><i class="fas fa-check-circle"></i> Ojos</p>
        <p><i class="fas fa-check-circle"></i> Retoque incluido</p> 
      </div>
    </div>

  <div id="promociones">
    <h1>Promociones diarias <i class="fas fa-medal" style="color:goldenrod;"></i></h1><br>
    <img src="img/promos.png">
  </div>

  <div id="contacto">
    <h1>Contactanos<i class="fas fa-address-book"></i></h1>
    <i class="fas fa-envelope-square"></i><p>lorrein1986@hotmail.com</p>
    <i class="fab fa-whatsapp"></i><p>655 288 789</p>
    <i class="fas fa-phone-square"></i><p>966 61 86 45</p>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3132.233901694742!2d-0.713563484667923!3d38.2740704796717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd63b655c43436db%3A0xaa6228f0b841479d!2sEstetica%20Azucena!5e0!3m2!1ses!2ses!4v1592335272660!5m2!1ses!2ses" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
  <div id="social">
      <h1>Social</h1>
      <a href="https://es-es.facebook.com/Centro-de-estetica-azucena-716387105111566/"><i class="fab fa-facebook"></i></a>
    </div>
</main>
</body>
</html>