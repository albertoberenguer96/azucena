$(document).ready(function(){

    $('.slider').bxSlider({//Slider de diapositivas
    mode: 'fade',
    captions: true,
    speed: 2500,
    pause: 6000,
    auto: true,
    autoControls: true
    });

    function irArriba(){
        $('.ir-arriba').click(function(){ //Boton con animacion para subir arriba
            $('body,html').animate({ 
                scrollTop:'0px' },1000); 
            });
        $(window).scroll(function(){
          if($(this).scrollTop() > 0){ 
              $('.ir-arriba').slideDown(600); 
            }else{ 
                $('.ir-arriba').slideUp(600); }
        });
      }

      irArriba();

      $('a[href^="#"]').click(function() {//scroll suave para anclas 
        var destino = $(this.hash);
        if (destino.length == 0) {
          destino = $('a[name="' + this.hash.substr(1) + '"]');
        }
        if (destino.length == 0) {
          destino = $('html');
        }
        $('html, body').animate({ scrollTop: destino.offset().top }, 500);//Duracion de la animacion 500 milisegundos
        return false;
      });
});

//Redireccionamiento
  function añadirCliente(){
    window.location.href="index.php?p=alta_clientes";
  }

  function listarClientes(){
    window.location.href="index.php?p=lista_clientes";
  }

  function añadirTratamiento(){
    window.location.href="index.php?p=alta_tratamientos";
  }

  function listarTratamientos(){
    window.location.href="index.php?p=lista_tratamientos";
  }

  function listaPedidos(){
    window.location.href="index.php?p=lista_pedidos";
  }

  function agregarPedido(){
    window.location.href="index.php?p=agregar_pedido";
  }

  function volver(){
    window.location.href="index.php?p=gestor";
  }

  function salir(){
    window.location.href="index.php?p=login";
  }

  function generarPDF(){
    window.location.href="listadoPDF.php";
  }