-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-03-2020 a las 10:53:50
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `azucena`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `cod_cli` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `poblacion` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`cod_cli`, `nombre`, `apellidos`, `poblacion`, `fecha_nac`) VALUES
('1', 'Jose', 'Berenguer', 'Elche', '19/02/1996'),
('2', 'Pepe', 'Marcelino', 'Alicante', '05/04/1998'),
('3', 'Pablo', 'Poveda', 'Madrid', '23/06/1985');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `cod_pedido` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `cod_cli` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `cod_tra` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_ped` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`cod_pedido`, `cod_cli`, `cod_tra`, `fecha_ped`) VALUES
('1', '2', '1', '2020-03-03 10:27:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamientos`
--

CREATE TABLE `tratamientos` (
  `cod_tra` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tratamientos`
--

INSERT INTO `tratamientos` (`cod_tra`, `descripcion`, `precio`) VALUES
('1', 'Manicura', '30'),
('2', 'Depilación comp', '65'),
('3', 'Pestañas', '50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cod_cli`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`cod_pedido`),
  ADD KEY `cod_cli` (`cod_cli`,`cod_tra`);

--
-- Indices de la tabla `tratamientos`
--
ALTER TABLE `tratamientos`
  ADD PRIMARY KEY (`cod_tra`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`cod_cli`) REFERENCES `clientes` (`cod_cli`),
  ADD CONSTRAINT `pedidos_ibfk_2` FOREIGN KEY (`cod_tra`) REFERENCES `tratamientos` (`cod_tra`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
