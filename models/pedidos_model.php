<?php
require_once('db_abstract_model.php');

class Pedido extends DBAbstractModel {

public $cod_pedido;
public $cod_cli;
public $cod_tra;
public $fecha_ped;

function __construct(){
	$this->db_name = 'azucena';
}

public function get($codigo_pedido=''){
	if($codigo_pedido != ''){
		$this->query = "
		SELECT *
		FROM pedidos
		WHERE cod_pedido = '$codigo_pedido'
		";

		$this->get_results_from_query();
	}else{
		$this->query = "
		SELECT 
			cod_pedido,
			c1.cod_cli,
			nombre,
			apellidos,
			descripcion,
			precio,
			fecha_ped
		FROM clientes c1
		INNER JOIN pedidos p1
			ON c1.cod_cli = p1.cod_cli
		INNER JOIN tratamientos t1
			ON t1.cod_tra = p1.cod_tra";
		$this->get_results_from_query();
	}

	if(count($this->rows) == 1){
		foreach ($this->rows[0] as $propiedad=>$valor){
			$this->$propiedad = $valor;
		}
	}
}

	public function set($pedido_data=array()) {
		if(array_key_exists('cod_pedido', $pedido_data)){
			$this->get($pedido_data['cod_pedido']); //leemos el cod_pedido por si existe, no crearlo de nuevo
			if($pedido_data['cod_pedido'] != $this->cod_pedido){
				foreach ($pedido_data as $campo=>$valor){
					$$campo = $valor;
				}
					$this->query = "
					INSERT INTO pedidos
					(cod_pedido,cod_cli,cod_tra,fecha_ped)
					VALUES
					('$cod_pedido','$cod_cli','$cod_tra','$fecha_ped')
					";
					$this->execute_single_query();
					$this->mensaje = 'El pedido <b>ha sido agregado exitosamente</b>';
			} else {
				$this->mensaje = 'El pedido ya existe';
			}
		} else {
			$this->mensaje = 'No se ha podido agregar el pedido';
		}
	}

	public function edit($pedido_data=array()) {
		foreach ($pedido_data as $campo=>$valor):
			$$campo = $valor;
		endforeach;
		$this->query = "
			UPDATE tratamientos
			SET cod_tra='$cod_tra',
			descripcion='$descripcion',
			precio='$precio'
			WHERE cod_tra = '$cod_tra'
		";
		$this->execute_single_query();
		$this->mensaje = 'Tratamiento modificado correctamente';

		
		}
	
	public function delete($codigo_pedido='') {
		$this->query = "
		DELETE FROM pedidos
		WHERE cod_pedido = '$codigo_pedido'
		";
		$this->execute_single_query();
		$this->mensaje = 'Pedido eliminado';
	}


}

?>