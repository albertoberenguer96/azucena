<?php
require_once('db_abstract_model.php');

class Tratamiento extends DBAbstractModel {

public $cod_tra;
public $descripcion;
public $precio;

function __construct(){
	$this->db_name = 'azucena';
}

public function get($codigo_tratamiento=''){
	if($codigo_tratamiento != ''){
		$this->query = "
		SELECT *
		FROM tratamientos
		WHERE cod_tra = '$codigo_tratamiento'
		";

		$this->get_results_from_query();
	}else{
		$this->query = "
		SELECT *
		FROM tratamientos";
		$this->get_results_from_query();
	}

	if(count($this->rows) == 1){
		foreach ($this->rows[0] as $propiedad=>$valor){
			$this->$propiedad = $valor;
		}
	}
}

	public function set($treatment_data=array()) {
		if(array_key_exists('cod_tra', $treatment_data)){
			$this->get($treatment_data['cod_tra']); //leemos el cod_tra por si existe, no crearlo de nuevo
			if($treatment_data['cod_tra'] != $this->cod_tra){
				foreach ($treatment_data as $campo=>$valor){
					$$campo = $valor;
				}
					$this->query = "
					INSERT INTO tratamientos
					(cod_tra,descripcion,precio)
					VALUES
					('$cod_tra','$descripcion','$precio')
					";
					$this->execute_single_query();
					$this->mensaje = $descripcion.' '.' <b>ha sido agregado exitosamente</b>';
			} else {
				$this->mensaje = 'El tratamiento ya existe';
			}
		} else {
			$this->mensaje = 'No se ha podido agregar el tratamiento';
		}
	}

	public function edit($treatment_data=array()) {
		foreach ($treatment_data as $campo=>$valor):
			$$campo = $valor;
		endforeach;
		$this->query = "
			UPDATE tratamientos
			SET cod_tra='$cod_tra',
			descripcion='$descripcion',
			precio='$precio'
			WHERE cod_tra = '$cod_tra'
		";
		$this->execute_single_query();
		$this->mensaje = 'Tratamiento modificado correctamente';

		
		}
		
	public function delete($cod_tra='') {
		$this->query = "
		DELETE FROM tratamientos
		WHERE cod_tra = '$cod_tra'
		";
		$this->execute_single_query();
		$this->mensaje = 'Tratamiento eliminado';
	}


}

?>