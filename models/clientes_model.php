<?php
require_once('db_abstract_model.php');

class Cliente extends DBAbstractModel {

public $cod_cli;
public $nombre;
public $apellidos;
public $poblacion;
public $fecha_nac;

function __construct(){
	$this->db_name = 'azucena';
}

public function get($codigo_cliente=''){
	if($codigo_cliente != ''){
		$this->query = "
		SELECT *
		FROM clientes
		WHERE cod_cli = '$codigo_cliente'
		";

		$this->get_results_from_query();

	}else{
		$this->query = "
		SELECT *
		FROM clientes";
		$this->get_results_from_query();
	}

	if(count($this->rows) == 1){
		foreach ($this->rows[0] as $propiedad=>$valor){
			$this->$propiedad = $valor;
		}
	}
}

	public function set($client_data=array()) {
		if(array_key_exists('cod_cli', $client_data)){
			$this->get($client_data['cod_cli']); //leemos el codigo del cliente por si existe, no crearlo de nuevo
			if($client_data['cod_cli'] != $this->cod_cli){
				foreach ($client_data as $campo=>$valor){
					$$campo = $valor;
				}
					$this->query = "
					INSERT INTO clientes
					(cod_cli,nombre,apellidos,poblacion,fecha_nac)
					VALUES
					('$cod_cli','$nombre','$apellidos','$poblacion','$fecha_nac')
					";
					$this->execute_single_query();
					$this->mensaje = "<b>$nombre $apellidos</b> ha sido agregado exitosamente";
			} else {
				$this->mensaje = 'Ya existe un cliente con el codigo cliente nº '.$client_data['cod_cli'];
			}
		} else {
			$this->mensaje = 'No se ha podido agregar el cliente';
		}
	}

	public function edit($client_data=array()) {
		foreach ($client_data as $campo=>$valor):
			$$campo = $valor;
		endforeach;
		$this->query = "
			UPDATE clientes
			SET nombre='$nombre',
			apellidos='$apellidos',
			poblacion='$poblacion',
			fecha_nac='$fecha_nac'
			WHERE cod_cli = '$cod_cli'
		";
		$this->execute_single_query();
		$this->mensaje = 'Cliente modificado correctamente';
		}

	public function delete($cod_cli='') {
		$this->query = "
		DELETE FROM clientes
		WHERE cod_cli = '$cod_cli'
		";
		$this->execute_single_query();
		$this->mensaje = 'Cliente eliminado';
	}


}

?>