<h2 align="center">Agregar Pedido</h2>
<?php
require("models/pedidos_model.php");
require("models/clientes_model.php");
require("models/tratamientos_model.php");

date_default_timezone_set('Europe/Madrid');
$fecha=date("Y-m-d H:i:s");

	$tratamiento=new Tratamiento();
	$tratamiento->get();
	$tratamientos = $tratamiento->get_rows();

	$cliente=new Cliente();
	$cliente->get();
	$clientes = $cliente->get_rows();

if(isset($_POST['agregar'])){
	$cod_pedido = $_POST['cod_pedido'];
	$cod_cli = $_POST['clientes'];
	$cod_tra = $_POST['tratamientos'];
	$fecha_ped = $_POST['fecha'];

	$pedido = new Pedido();
		$pedido_data = array('cod_pedido'=>$cod_pedido,'cod_cli'=>$cod_cli,'cod_tra'=>$cod_tra,'fecha_ped'=>$fecha_ped);

		$pedido->set($pedido_data);
		echo $pedido->mensaje;
}
	
?>
<form action="<?php $_PHP_SELF ?>" method="POST">
<label>Codigo de pedido:</label><input type="text" name="cod_pedido">
<label>Cliente:</label>
<select name="clientes" id="clientes">
<?php
foreach ($clientes as $key => $value) {
	echo "<option value=".$value['cod_cli'].">".utf8_encode($value['nombre'])." ".$value['apellidos']."</option>";
}
?>
</select>

<label>Tratamiento:</label>
<select name="tratamientos" id="tratamientos">
<?php
foreach ($tratamientos as $key => $value) {
	echo "<option value=".$value['cod_tra'].">".utf8_encode($value['descripcion'])."</option>";
}
?>
</select>
<label>Fecha Pedido:</label><input type="datetime" value="<?= $fecha?>" name="fecha">

<input type="submit" name="agregar" value="Agregar Pedido">
</form>
<button class="volver" onclick="volver()">Volver</button>