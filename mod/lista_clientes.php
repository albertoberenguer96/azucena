<h2 align="center">Lista de Clientes</h2>
<?php
require("models/clientes_model.php");

	$cliente=new Cliente();
	$cliente->get();
	$clientes = $cliente->get_rows();

	if(isset($_GET['borrar'])){
	echo "<p align='center'>¿Esta seguro de que desea borrar el pedido ".$_GET['borrar']."?</p>";
	echo "<div id='sino'><a href = 'index.php?p=lista_clientes&borrando=".$_GET['borrar']."'><button class='sino'>SI</button></a>";
	echo "<a href = 'index.php?p=lista_clientes'><button class='sino'>NO</button></a></div>";
}

if(isset($_GET['borrando'])){
	$cliente->delete($_GET['borrando']);
	header("location:index.php?p=lista_clientes");
}
?>
<table>
	<tr>
		<th>Codigo Cliente</th><th>Nombre</th><th>Apellidos</th><th>Poblacion</th><th>Fecha nacimiento</th>
	</tr>
<?php
foreach ($clientes as $key => $value) {
	echo "<tr>";
	echo "<td>".$value['cod_cli']."</td>";
	echo "<td>".utf8_encode($value['nombre'])."</td>";
	echo "<td>".utf8_encode($value['apellidos'])."</td>";
	echo "<td>".utf8_encode($value['poblacion'])."</td>";
	echo "<td>".$value['fecha_nac']."</td>";
?>
	<td><a href ='index.php?p=lista_clientes&borrar=<?php echo $value['cod_cli'] ?>'><img src='img/borrar.png' width='20px'></a></td>

<?php
}
?>

</tr>
</table>
<button class="volver" onclick="volver()">Volver</button>