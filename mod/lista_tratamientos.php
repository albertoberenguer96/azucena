<h2 align="center">Lista de Tratamientos</h2>
<?php
require("models/tratamientos_model.php");

	$tratamiento=new Tratamiento();
	$tratamiento->get();
	$tratamientos = $tratamiento->get_rows();

	if(isset($_GET['borrar'])){
	echo "<p align='center'>¿Esta seguro de que desea borrar el tratamiento ".$_GET['borrar']."?</p>";
	echo "<div id='sino'><a href = 'index.php?p=lista_tratamientos&borrando=".$_GET['borrar']."'><button class='sino'>SI</button></a>";
	echo "<a href = 'index.php?p=lista_tratamientos'><button class='sino'>NO</button></a></div>";
}

if(isset($_GET['borrando'])){
	$tratamiento->delete($_GET['borrando']);
	header("location:index.php?p=lista_tratamientos");
}
?>
<table>
	<tr>
		<th>Codigo Tratamiento</th><th>Descripcion</th><th>Precio</th>
	</tr>
<?php
foreach ($tratamientos as $key => $value) {
	echo "<tr>";
	echo "<td>".$value['cod_tra']."</td>";
	echo "<td>".utf8_encode($value['descripcion'])."</td>";
	echo "<td>".$value['precio']."€"."</td>";
?>
	<td><a href ='index.php?p=lista_tratamientos&borrar=<?php echo $value['cod_tra'] ?>'><img src='img/borrar.png' width='20px'></a></td>

<?php
}
?>

</tr>
</table>
<button class="volver" onclick="volver()">Volver</button>