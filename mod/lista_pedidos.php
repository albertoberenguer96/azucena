<h2 align="center">Lista de Pedidos</h2>
<?php
require("models/pedidos_model.php");

	$pedido=new Pedido();
	$pedido->get();
	$pedidos = $pedido->get_rows();

if(isset($_GET['borrar'])){
	echo "<p align='center'>¿Esta seguro de que desea borrar el pedido ".$_GET['borrar']."?</p>";
	echo "<div id='sino'><a href = 'index.php?p=lista_pedidos&borrando=".$_GET['borrar']."'><button class='sino'>SI</button></a>";
	echo "<a href = 'index.php?p=lista_pedidos'><button class='sino'>NO</button></a></div>";
}

if(isset($_GET['borrando'])){
	$pedido->delete($_GET['borrando']);
	header("location:index.php?p=lista_pedidos");
}	
?>
<table>
	<tr>
		<th>Codigo Pedido</th><th>Codigo Cliente</th><th>Nombre</th><th>Apellidos</th><th>Descripcion</th><th>Precio</th><th>Fecha pedido</th>
	</tr>
<?php
foreach ($pedidos as $key => $value) {
	echo "<tr>";
	echo "<td>".$value['cod_pedido']."</td>";
	echo "<td>".$value['cod_cli']."</td>";
	echo "<td>".utf8_encode($value['nombre'])."</td>";
	echo "<td>".utf8_encode($value['apellidos'])."</td>";
	echo "<td>".utf8_encode($value['descripcion'])."</td>";
	echo "<td>".$value['precio']."€"."</td>";
	echo "<td>".$value['fecha_ped']."</td>";
?>
	<td><a href ='index.php?p=lista_pedidos&borrar=<?php echo $value['cod_pedido'] ?>'><img src='img/borrar.png' width='20px'></a></td>
<?php
}
?>

</tr>
</table>
<button class="pdf" onclick="generarPDF()">Generar PDF</button><br>
<button class="volver" onclick="volver()">Volver</button>