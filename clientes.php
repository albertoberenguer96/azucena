<?php
require("alumnos_model.php");

$alumno=new Alumno();
$alumno->get();
$alumnos = $alumno->get_rows();

if(isset($_GET['borrar'])){
	echo "¿Esta seguro de que desea borrar el alumno ".$_GET['borrar']."?";
	echo "<a href = 'index.php?p=alumnos&borrando=".$_GET['borrar']."'><button>SI</button></a>";
	echo "<a href = 'index.php?p=alumnos'><button>NO</button></a>";
}

if(isset($_GET['borrando'])){
	$alumno->delete($_GET['borrando']);
	header("location:index.php?p=alumnos");
}

?>
<table>
	<tr>
		<th>id</th><th>nombre</th><th>apellido</th><th>telefono</th><th>email</th><th>borrar</th><th>modificar</th>
	</tr>
<?php
foreach ($alumnos as $key => $value) {
	echo "<tr>";
	echo "<td>".$value['id']."</td>";
	echo "<td>".$value['nombre']."</td>";
	echo "<td>".$value['apellido']."</td>";
	echo "<td>".$value['telefono']."</td>";
	echo "<td>".$value['email']."</td>";

?>
	<td><a href ='index.php?p=alumnos&borrar=<?php echo $value['id'] ?>'><img src='images/borrar.jpg' width='20px'></a></td>
	<td><a href ='index.php?p=modificar_alumno'><img src='images/modificar.gif' width='20px'></a></td>

<?php
}
?>

</tr>
</table>

<a href="index.php?p=alta_alumnos"><button>Alta Alumnos</button></a>
<?php
	
?>