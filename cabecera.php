<?php
session_start();
 define("user","admin");
 define("pass","admin");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Centro de Estética Azucena</title>

  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'>
  <script src="https://kit.fontawesome.com/c0abeb4c98.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
  <script src="js/script.js"></script>
</head>

<body>
  <header>
      <img class="logo" src="img/logo.png">
    <nav>
      <ul>
        <li><a href="index.php?p=inicio">Inicio</a></li>
        <li><a href="#services">Servicios</a></li>
        <li><a href="#promociones">Promociones</a></li>
        <li><a href="#contacto">Contacto</a></li>
        <li><a href="index.php?p=login">Identificarse</a></li>
      </ul>
    </nav>
  </header>